/* eslint-disable arrow-parens */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FaGithubAlt, FaPlus, FaSpinner } from 'react-icons/fa';
import Container from '../../Components/Container/index';
import { Form, SubmitButton, List } from './styles';
import api from '../../services/api';

export default function Main() {
  const [newRepo, setRepo] = useState('');
  const [repositories, setRepositories] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const repositoriesStorage = localStorage.getItem('repositories');
    if (repositoriesStorage) {
      setRepositories(JSON.parse(repositoriesStorage));
    }
  }, []);
  const handleInputChange = e => {
    console.log(e.target.value);
    setRepo(e.target.value);
    setError(false);
  };

  const handleSubmit = async e => {
    e.preventDefault();
    setLoading(true);
    setError(false);

    try {
      if (newRepo === '') throw 'Você precisa indicar um repositório';

      const hasRepo = repositories.find(r => r.name === newRepo);

      if (hasRepo) throw 'Repostório duplicado';

      const response = await api.get(`/repos/${newRepo}`);
      console.log(response.data);

      const data = {
        name: response.data.full_name,
      };

      setRepositories([...repositories, data]);
      setRepo('');
      localStorage.setItem(
        'repositories',
        JSON.stringify([...repositories, data])
      );
    } catch (e) {
      console.log(e);
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container>
      <h1>
        <FaGithubAlt />
        Repositórios
      </h1>

      <Form onSubmit={handleSubmit} error={error}>
        <input
          type="text"
          placeholder="Adicionar repositório"
          value={newRepo}
          onChange={handleInputChange}
        />

        <SubmitButton loading={loading}>
          {loading ? (
            <FaSpinner color="#fff" size={14} />
          ) : (
              <FaPlus color="#FFF" size={14} />
            )}
        </SubmitButton>
      </Form>

      <List>
        {repositories.map(repository => (
          <li key={repository.name} className="">
            <span> {repository.name}</span>
            <Link to={`/repository/${encodeURIComponent(repository.name)}`}>
              Detalhes
            </Link>
          </li>
        ))}
      </List>
    </Container>
  );
}
